import Vue from 'vue';
import Vuex from 'vuex'; // vuex es solo para manejar el state de la aplicacion


Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        locationsDelivery:[],
        locationsTakeaway:[],
        categories:[],
        todoMenu:[],
        menuTradicionales:[],
        menuRecomendaciones:[],
        menuParaCompartir:[]
        
    },
    mutations: {
        AGREGAR_RESTAURANTES_DELIVERY(state, locationsDelivery) {
            state.locationsDelivery = locationsDelivery.data;
        },
        AGREGAR_RESTAURANTES_TAKEAWAY(state,locationsTakeaway){
            state.locationsTakeaway = locationsTakeaway.data;
        },
        AGREGAR_CATEGORIAS_MENU(state, categories) {
            state.categories = categories;
        },
        AGREGAR_TODO_MENU(state, todoMenu){
            state.todoMenu = todoMenu.data;
        },
        AGREGAR_MENU_TRADICIONALES(state, menuTradicionales){
            state.menuTradicionales = menuTradicionales.data;
        },
        AGREGAR_MENU_RECOMENDACIONES(state, menuRecomendaciones){
            state.menuRecomendaciones = menuRecomendaciones.data;
        },
        AGREGAR_MENU_PARA_COMPARTIR(state, menuParaCompartir){
            state.menuParaCompartir = menuParaCompartir.data;
        }
        
    },
    getters: {
        obtenerRestaurantesDelivery: state => {
            return state.locationsDelivery.data;
        },
        obtenerRestaurantesTakeaway: state => {
            return state.locationsTakeaway.data;
        },
        obtenerCategoriasMenu: state => {
            return state.categories;
        }
        
    }

});