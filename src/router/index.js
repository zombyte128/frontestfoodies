import Vue from 'vue';
import VueRouter from 'vue-router';

import MainComponent from '../components/MainComponent'
import MenuComponent from '../components/MenuComponent'

const routes = [
    {
      path:'/',
      component:MainComponent,
      name:'MainComponent'
    },
    {
      path:'/menu',
      component:MenuComponent,
      name:'MenuComponent'
    }
  ]

const router = new VueRouter({
    mode: 'history', //es para quita el # que le agrega vue añ user vue router a la url
    routes,
    linkActiveClass: "active",

});

Vue.use(VueRouter);
export default router;