/* eslint-env es6 */
/* eslint-disable */ 
$(document).ready(function() {

/*============================================================================================*/
/* Animacion scroll del menu
/*============================================================================================*/
var scrolltoOffset = $("#header").outerHeight() + 30;
$(document).on("click", ".nav-menu a, .mobile-nav a, .scrollto", function (
    e
) {
    if (
        location.pathname.replace(/^\//, "") ==
            this.pathname.replace(/^\//, "") &&
        location.hostname == this.hostname
    ) {
        var target = $(this.hash);
        if (target.length) {
            e.preventDefault();

            var scrollto = target.offset().top - scrolltoOffset;

            if ($(this).attr("href") == "#header") {
                f;
                scrollto = 0;
            }

            $("html, body").animate(
                {
                    scrollTop: scrollto,
                },
                1500,
                "easeInOutExpo"
            );
            if ($(this).parents(".nav-menu, .mobile-nav").length) {
                $(".nav-menu .active, .mobile-nav .active").removeClass(
                    "active"
                );
                $(this).closest("li").addClass("active");
            }
            if ($("body").hasClass("mobile-nav-active")) {
                $("body").removeClass("mobile-nav-active");
                $("#app").removeClass("filtro");
                $(".mobile-nav-toggle i").toggleClass(
                    "icofont-navigation-menu icofont-close fas fa-times fas"
                );
                $(".mobile-nav-overly").fadeOut();
            }
            return false;
        }
    }
});

/*============================================================================================*/
/* Menu Version mobile
/*============================================================================================*/
if ($(".nav-menu").length) {
    var $mobile_nav = $(".nav-menu").clone().prop({
        class: "mobile-nav d-lg-none",
    });
    $("body").append($mobile_nav);
    $("body").prepend(
        '<button type="button" class="mobile-nav-toggle d-lg-none"><i class="fas fa-bars"></i></button>'
    );
    $("body").append('<div class="mobile-nav-overly"></div>');

    $(document).on("click", ".mobile-nav-toggle", function (e) {
        $("body").toggleClass("mobile-nav-active");
        $("#app").toggleClass("filtro");
        $(".mobile-nav-toggle i").toggleClass(
            "icofont-navigation-menu icofont-close fas fa-times fas"
        );
        $(".mobile-nav-overly").toggle();
    });

    $(document).click(function (e) {
        var container = $(".mobile-nav, .mobile-nav-toggle");
        if (
            !container.is(e.target) &&
            container.has(e.target).length === 0
        ) {
            if ($("body").hasClass("mobile-nav-active")) {
                $("body").removeClass("mobile-nav-active");
                $(".mobile-nav-toggle i").toggleClass(
                    "icofont-navigation-menu icofont-close fas fa-times fas"
                );
                $(".mobile-nav-overly").fadeOut();
            }
        }
    });
} else if ($(".mobile-nav, .mobile-nav-toggle").length) {
    $(".mobile-nav, .mobile-nav-toggle").hide();
}
var nav_sections = $("section");
var main_nav = $(".nav-menu, #mobile-nav");

$(window).on("scroll", function () {
    var cur_pos = $(this).scrollTop() + 200;

    nav_sections.each(function () {
        var top = $(this).offset().top,
            bottom = top + $(this).outerHeight();

        if (cur_pos >= top && cur_pos <= bottom) {
            if (cur_pos <= bottom) {
                main_nav.find("li").removeClass("active");
            }
            main_nav
                .find('a[href="#' + $(this).attr("id") + '"]')
                .parent("li")
                .addClass("active");
        }
        if (cur_pos < 300) {
            $(".home a").addClass("activo");
        }
        if(cur_pos == 200) {
            $(".home a").removeClass("activo");
        }
    });
});

/*============================================================================================*/
/* Mostrar menu flotante despues de hacer scroll
/*============================================================================================*/
$(window).scroll(function () {
    if ($(this).scrollTop() > 10) {
        $("#header").addClass("header-scrolled");
    }else{
        $("#header").removeClass("header-scrolled");
    }
});
if ($(window).scrollTop() > 10) {
    $("#header").addClass("header-scrolled");
}

/*============================================================================================*/
/* Mostrar menu flotante despues de hacer scroll v2
/*============================================================================================*/
$(window).scroll(function () {
    if ($(this).scrollTop() > 10) {
        $("#headers").addClass("header-scrolled2");
    }else{
        $("#headers").removeClass("header-scrolled2");
    }
});
if ($(window).scrollTop() > 10) {
    $("#headers").addClass("header-scrolled2");
}
});